﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficLight {
    class LightFunctionalitiesImpl : LightFunctionalities {
        public void TurnOn(LightTower lightTower, Light light) {
            light.isTurnedOn = true;
            lightTower.turnedOnLightList.Add(light);
        }

        public void TurnOff(LightTower lightTower, Light light) {
            light.isTurnedOn = false;
            lightTower.turnedOnLightList.Remove(light);
        }
    }
}
