﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficLight {
    interface LightFunctionalities {
        void TurnOn(LightTower lightTower, Light light);
        void TurnOff(LightTower lightTower, Light light);
    }
}
