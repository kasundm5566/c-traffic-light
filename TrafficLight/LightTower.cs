﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficLight {
    class LightTower {
        public Light redLight { get; set; }
        public Light yellowLight { get; set; }
        public Light greenLight { get; set; }
        public List<Light> turnedOnLightList = new List<Light>();

        public LightTower(Light redLight, Light yellowLight, Light greenLight) {
            this.redLight = redLight;
            this.yellowLight = yellowLight;
            this.greenLight = greenLight;
            turnedOnLightList = new List<Light>();
        }

        public List<Light> CurrentlyTurnedOnLights() {
            return turnedOnLightList;
        }
    }
}
