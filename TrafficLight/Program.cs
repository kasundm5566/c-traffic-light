﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficLight {
    class Program {
        static void Main(string[] args) {
            bool isInputValid = false;
            int loopCount = 1;
            while (!isInputValid) {
                Console.Write("How many times to loop lights: ");                
                isInputValid = int.TryParse(Console.ReadLine(), out loopCount);
            }

            Light redLight = new Light(Light.Colors.Red);
            Light yellowLight = new Light(Light.Colors.Yellow);
            Light greenLight = new Light(Light.Colors.Green);

            LightTower lightTower = new LightTower(redLight, yellowLight, greenLight);

            LightFunctionalities lightFunctionalities = new LightFunctionalitiesImpl();

            for (int i = 0; i < loopCount; i++) {
                lightFunctionalities.TurnOn(lightTower, redLight);
                printTurnedOnLights(lightTower.turnedOnLightList);

                lightFunctionalities.TurnOn(lightTower, yellowLight);
                printTurnedOnLights(lightTower.turnedOnLightList);

                lightFunctionalities.TurnOff(lightTower, redLight);
                lightFunctionalities.TurnOff(lightTower, yellowLight);

                lightFunctionalities.TurnOn(lightTower, greenLight);
                printTurnedOnLights(lightTower.turnedOnLightList);

                lightFunctionalities.TurnOff(lightTower, greenLight);

                lightFunctionalities.TurnOn(lightTower, yellowLight);
                printTurnedOnLights(lightTower.turnedOnLightList);

                lightFunctionalities.TurnOff(lightTower, yellowLight);
            }
            Console.WriteLine("Press Enter to exit.");
            Console.ReadLine();
        }

        private static void printTurnedOnLights(List<Light> turnedOnLightList) {
            for (int i = 0; i < turnedOnLightList.Count; i++) {
                switch (turnedOnLightList.ElementAt(i).color) {
                    case Light.Colors.Red:
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write(turnedOnLightList.ElementAt(i).color);
                        Console.ResetColor();
                        break;
                    case Light.Colors.Yellow:
                        Console.BackgroundColor = ConsoleColor.Yellow;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write(turnedOnLightList.ElementAt(i).color);
                        Console.ResetColor();
                        break;
                    case Light.Colors.Green:
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write(turnedOnLightList.ElementAt(i).color);
                        Console.ResetColor();
                        break;
                    default:
                        break;
                }              
            }
            Console.WriteLine();
        }
    }
}
