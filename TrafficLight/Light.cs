﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficLight {
    class Light {
        public enum Colors {
            Red,
            Yellow,
            Green
        };

        public Colors color { get; set; }
        public bool isTurnedOn { get; set; }

        public Light(Colors color, bool isTurnedOn = false) {
            this.color = color;
            this.isTurnedOn = isTurnedOn;
        }
    }
}
